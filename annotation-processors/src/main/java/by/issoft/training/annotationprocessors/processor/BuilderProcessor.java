package by.issoft.training.annotationprocessors.processor;

import by.issoft.training.annotationprocessors.annotation.Builder;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@SupportedAnnotationTypes("by.issoft.training.annotationprocessors.annotation.Builder")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class BuilderProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Map<TypeElement, List<VariableElement>> classFieldsMap = new HashMap<>();

        for (final Element element : roundEnv.getElementsAnnotatedWith(Builder.class)) {
            if (element instanceof TypeElement) {
                final TypeElement typeElement = (TypeElement) element;
                List<VariableElement> fields = new ArrayList<>();

                for (final Element eclosedElement : typeElement.getEnclosedElements()) {
                    if (eclosedElement instanceof VariableElement) {
                        final VariableElement variableElement = (VariableElement) eclosedElement;
                        fields.add(variableElement);
                    }
                }
                classFieldsMap.put(typeElement, fields);
            }
        }

        try {
            writeBuilderFile(classFieldsMap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    private void writeBuilderFile(Map<TypeElement, List<VariableElement>> classFieldsMap) throws IOException {
        for (Map.Entry<TypeElement, List<VariableElement>> entry : classFieldsMap.entrySet()) {
            TypeElement typeElement = entry.getKey();
            List<VariableElement> fields = entry.getValue();
            String fullClassName = typeElement.getQualifiedName().toString();
            String className = typeElement.getSimpleName().toString();
            String packageName = fullClassName.substring(0, fullClassName.lastIndexOf('.'));
            String builderFullClassName = fullClassName + "Builder";
            String builderClassName = className + "Builder";

            System.out.println(fullClassName);
            System.out.println(className);
            System.out.println(packageName);

            JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(builderFullClassName);
            try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();

                out.print("public class ");
                out.print(builderClassName);
                out.println(" {");
                out.println();

                out.print("private ");
                out.print(className);
                out.print(" obj = new ");
                out.print(className);
                out.print("();");
                out.println();

                for (VariableElement field : fields) {
                    String fullType = field.asType().toString();
                    String type = fullType.substring(fullType.lastIndexOf('.') + 1);
                    String fieldName = field.getSimpleName().toString();
                    String methodName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    out.print("public void build");
                    out.print(methodName);
                    out.print("(");
                    out.print(type + " " + fieldName);
                    out.print(") {");
                    out.println();
                    out.println("   obj.set" + methodName + "(" + fieldName + ");");
                    out.println("}");
                }

                out.print("public ");
                out.print(className);
                out.print(" build() {");
                out.print(" return obj; ");
                out.println("}");

                out.println("}");
            }
        }
    }
}
